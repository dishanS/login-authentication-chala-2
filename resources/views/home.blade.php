@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8" style="padding: 100px">
                <div class="card bg-dark" style="color: white">
                    <div class="card-header" style="color: black;background-color: white">
                        <p style="text-align: center;font-size: 20px">{{ __('Home Page') }}</p>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div style="padding-left: 30px;text-align: center">

                            <div class="row" style="padding-bottom: 30px">
                                <p>{{ __('You are logged in!') }}</p>
                            </div>
                            <div class="row">
                                <label for=" "> User Name :</label>
                                <p> {{ Auth::user()->name }}</p>
                            </div>
                            <div class="row" style="padding-top: 5px">
                                <label for=" "> User Name :</label>
                                <p> {{ Auth::user()->email }}</p>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
