<!DOCTYPE html>
<html lang="en" class="h-100">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Login Authentication Skills Test</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        .btn-secondary,
        .btn-secondary:hover,
        .btn-secondary:focus {
            color: #333;
            text-shadow: none;
        }

        body {
            text-shadow: 0 .05rem .1rem rgba(0, 0, 0, .5);
            box-shadow: inset 0 0 5rem rgba(0, 0, 0, .5);
        }

        .cover-container {
            max-width: 42em;
        }

        .nav-masthead .nav-link {
            padding: .25rem 0;
            font-weight: 700;
            color: rgba(255, 255, 255, .5);
            background-color: transparent;
            border-bottom: .25rem solid transparent;
        }

        .nav-masthead .nav-link:hover,
        .nav-masthead .nav-link:focus {
            border-bottom-color: rgba(255, 255, 255, .25);
        }

        .nav-masthead .nav-link+.nav-link {
            margin-left: 1rem;
        }

        .nav-masthead .active {
            color: #fff;
            border-bottom-color: #fff;
        }

    </style>
</head>

<body class="d-flex h-100 text-center text-white bg-dark">

    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
        <header class="mb-auto">

        </header>

        <main class="px-3">
            <h1>Login Authentication Skills Test</h1>

            <div class="row justify-content-center" style="padding-top: 25px">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <div style="padding-bottom: 15px">
                                <a href="{{ route('login') }}"
                                    class="btn btn-lg btn-secondary fw-bold border-white" 
                                    style="color: black;width: 100%;">Log
                                    in</a>
                            </div>
                            <div> 
                                <a href="{{ route('register') }}"
                                    class="btn btn-lg btn-secondary fw-bold border-white"
                                    style="color: black ;width: 100%"">Register</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </main>
        <footer class="mt-auto text-white-50">
            <p>Copyright &copy; 2022</p>
        </footer>
    </div>
</body>

</html>
